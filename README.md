# Frontend Mentor - Four card feature section solution

This is a solution to the [Four card feature section challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/four-card-feature-section-weK1eFYK). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - Four card feature section solution](#frontend-mentor---four-card-feature-section-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
      - [Mobile Layout](#mobile-layout)
      - [Desktop Layout](#desktop-layout)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
  - [Author](#author)
## Overview

### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size

### Screenshot
#### Mobile Layout
![Mobile Layout](./assets/solution-images/solution-mobile.jpeg)
#### Desktop Layout
![Desktop Layout](./assets/solution-images/solution-desktop.jpeg)

### Links

- Solution URL: [View solution code here](https://gitlab.com/deejayjay/frontend-mentor-four-card-feature-section)
- Live Site URL: [View live site here](https://deejayjay-fem-four-card-feat-section.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Grid Template Areas
- Mobile-first workflow
- Responsive Web Design
- Media Queries

## Author

- Github - [DeeJayJay](github.com/deejayjay)
- Frontend Mentor - [@deejayjay](https://www.frontendmentor.io/profile/deejayjay)
- Twitter - [@deejay_the_dev](https://twitter.com/deejay_the_dev)
